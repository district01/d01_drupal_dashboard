<?php

namespace Drupal\d01_drupal_dashboard;

/**
 * Interface DashboardServiceInterface.
 *
 * @package Drupal\d01_drupal_dashboard
 */
interface DashboardServiceInterface {

  /**
   * Function to retrieve content for given user.
   *
   * @param bool $current_user
   *   Content for given or other users.
   *
   * @return array
   *   Returns table with recent content.
   */
  public function getRecentContent($current_user = FALSE);

  /**
   * Get currently active user info.
   *
   * @return array
   *   Returns array containing user info.
   */
  public function getUserInfo();

  /**
   * Get content creation shortcuts.
   *
   * @return array
   *   Returns array containing content management links.
   */
  public function getContentShortcuts();

  /**
   * Function to build quick shortcuts.
   *
   * @return array
   *   Returns array with quick shortcuts.
   */
  public function getQuickShortcuts();

}
