<?php

namespace Drupal\d01_drupal_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\d01_drupal_dashboard\DashboardService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * D01 User Dashboard controller.
 *
 * @package Drupal\d01_drupal_dashboard\Controller
 */
class DashboardController extends ControllerBase {

  /**
   * The dashboard service.
   *
   * @var \Drupal\d01_drupal_dashboard\DashboardService
   */
  protected $dashboard;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('d01_drupal_dashboard.dashboard')
    );
  }

  /**
   * DashboardController constructor.
   *
   * @param \Drupal\d01_drupal_dashboard\DashboardService $dashboardService
   *   Dashboard service for user data.
   */
  public function __construct(DashboardService $dashboardService) {
    $this->dashboard = $dashboardService;
  }

  /**
   * Build user dashboard.
   */
  public function buildUserDashboard() {
    return [
      '#type' => 'd01_drupal_dashboard',
      '#user_content' => $this->buildRecentContentTable(TRUE),
      '#recent_content' => $this->buildRecentContentTable(),
      '#user_info' => $this->dashboard->getUserInfo(),
      '#content_shortcuts' => $this->dashboard->getContentShortcuts(),
      '#shortcuts' => $this->dashboard->getQuickShortcuts(),
    ];
  }

  /**
   * Function to build a table containing user content.
   *
   * @param bool $current_user
   *   Retrieve table for current user or not.
   *
   * @return array
   *   Returns table containing recent content.
   */
  protected function buildRecentContentTable($current_user = FALSE) {
    // Dashboard content.
    $nodes = $this->dashboard->getRecentContent($current_user);

    // Default table headers.
    $title = $current_user ? t('Your recent content') : t('Recent content others');
    $headers = [$title, t('Type'), t('Edit'), t('Delete')];

    // Build table data.
    $recent_content = [];
    if ($nodes) {
      foreach ($nodes as $index => $node) {
        $recent_content[$index]['view'] = Link::fromTextAndUrl($node->label(), Url::fromRoute('entity.node.canonical', ['node' => $node->id()]));
        $recent_content[$index]['type'] = $node->getType();
        $recent_content[$index]['edit'] = Link::fromTextAndUrl('Edit', Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]));
        $recent_content[$index]['delete'] = Link::fromTextAndUrl('Delete', Url::fromRoute('entity.node.delete_form', ['node' => $node->id()]));
      }
    }
    else {
      $headers = [$title];
      $recent_content[] = [t('Looks like no content has been created yet')];
    }

    return [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $recent_content,
    ];
  }

}
