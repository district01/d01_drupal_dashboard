<?php

namespace Drupal\d01_drupal_dashboard;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DashboardService.
 *
 * @package Drupal\d01_drupal_dashboard
 */
class DashboardService implements DashboardServiceInterface {

  /**
   * Current logged in user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * Dashboard constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current logged in user.
   */
  public function __construct(AccountInterface $current_user) {
    $this->user = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecentContent($current_user = FALSE) {
    // Get user content.
    $type = $current_user ? '=' : '!=';
    $user_content = \Drupal::entityQuery('node')->condition('uid', $this->user->id(), $type);
    $nodes = Node::loadMultiple($user_content->execute());
    $nodes = array_slice($nodes, 0, 10);

    return $nodes;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserInfo() {
    $user_info['name'] = t('Welcome @username', ['@username' => $this->user->getAccountName()]);
    $user_info['mail'] = $this->user->getEmail();
    $user_info['edit'] = Url::fromRoute('entity.user.edit_form', ['user' => $this->user->id()]);
    return $user_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentShortcuts() {
    // Get all content types.
    $content_types = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();

    // Build links if access.
    $shortcuts = [];
    foreach ($content_types as $index => $type) {
      $url = Url::fromRoute('node.add', ['node_type' => $type->get('type')]);
      if ($url->access($this->user)) {
        $shortcuts[$index] = Link::fromTextAndUrl($type->get('name'), $url);
      }
    }

    // Alphabetically.
    sort($shortcuts);

    return $shortcuts;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuickShortcuts() {
    // Quick shortcuts management routes.
    $routes = [
      'locale.translate_page' => 'Manage translations',
      'entity.menu.collection' => 'Manage menu\'s',
      'entity.webform.collection' => 'Manage webforms',
      'entity.taxonomy_vocabulary.collection' => 'Manage taxonomies',
      'entity.user.collection' => 'Manage people',
    ];

    // Build links if access.
    $shortcuts = [];
    foreach ($routes as $index => $route_text) {
      if (Url::fromRoute($index)->access($this->user)) {
        $shortcuts[] = Link::fromTextAndUrl($route_text, Url::fromRoute($index));
      }
    }

    return $shortcuts;
  }

}
