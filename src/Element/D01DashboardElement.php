<?php

namespace Drupal\d01_drupal_dashboard\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * D01 share on clipboard element.
 *
 * @RenderElement("d01_drupal_dashboard")
 */
class D01DashboardElement extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_dashboard',
      '#attributes' => [
        'class' => '',
      ],
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      '#user_content' => [],
      '#recent_content' => [],
      '#user_info' => [],
      '#content_shortcuts' => [],
      '#shortcuts' => [],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderElement($element) {
    $element['#attached']['library'][] = 'd01_drupal_dashboard/dashboard';
    $element['user_content'] = $element['#user_content'];
    $element['recent_content'] = $element['#recent_content'];
    $element['user_info'] = $element['#user_info'];
    $element['content_shortcuts'] = $element['#content_shortcuts'];
    $element['shortcuts'] = $element['#shortcuts'];
    return $element;
  }

}
